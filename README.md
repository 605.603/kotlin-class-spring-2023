# 605.603 Object-Oriented and Functional Programming in Kotlin (Spring 2023)

## Midterm Exam

March 9, 2023 730p-930p Eastern

Covers content of weeks 1-6

## Final Exam

May 4, 2023 730p-930p EST

Covers content of weeks 8-13

## Spring Break

No office hours on March 21
No class on March 23

## Videos

Class sessions are recorded and videos are posted here shortly after.

   * Week 1: https://youtu.be/lPZSuoTyIo8
   * Week 2: TBD
   * Week 3: TBD
   * Week 4: TBD
   * Week 5: TBD
   * Week 6: TBD
   * Week 7: (Midterm; no video)
   * Week 8: TBD
   * Week 9: TBD
   * Week 10: TBD
   * Week 11: TBD
   * Week 12: TBD
   * Week 13: TBD
   * Week 14: (Final; no video)

## Assignments

Assignments are posted after class sessions. Assignments are one-week, and there will be 8-10 assignments (depending on how topics are presented)

## Topics

This is the general order of topics to be presented, but tends to change a bit while the term is progressing. Sometimes I'll go deeper into some topics, add new topics, and may delay or skip others.

   * Class Logistics and Introduction
   * Kotlin Playground
   * IntelliJ Setup
   * Gradle Basics
   * Variables, Values and Data Types
   * Expressions, Part I
   * Basic Functions
   * Statements
   * Labels
   * Higher-Order Functions
   * Lambdas
   * Intro to Object-Oriented Programming Concepts
   * Classes
   * Packages
   * Class Inheritance
   * Inner classes
   * Extension Functions
   * Nullability
   * Expressions, Part II
   * Scoping Functions
   * Common Collection Data Structures
   * Expressions, Part III
   * Functions on Data Structures
   * Interfaces
   * Objects
   * Class Delegation and Composition
   * Operator Overloading for use as Data Structures
   * Coding Assignments
   * Generics
   * Enumeration Types
   * Sealed Classes
   * Exceptions
   * File I/O
   * Late Variable Initialization
   * Property Delegation
   * Basic Functional Programming Concepts
   * Basic Functional Programming in Kotlin
   * Mixing Functional and Object-Oriented Programming
   * Builders and Domain-Specific Languages
   * Multitasking Concepts
   * Basic Threads and Executors
   * Coroutines
