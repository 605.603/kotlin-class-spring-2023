package week0101

fun main() {
    // prefer val when possible
    val x: Int = 10
    val y: Int = 20
    println(x + y)

    val x2 = 10
    val y2 = 20
    println(x2 + y2)

    val done = false
    val isPerson = true
    val result1 = done && isPerson // AND - short-circuiting
    val result2 = done || isPerson // OR - short-circuiting

    val xAndYAreEqual: Boolean = (x == y)
    val xAndYAreNotEqual: Boolean = (x != y)

    var n = 0
    n++ // post-increment
    ++n // pre-increment
    n += 2 // same as n = n + 2

    // don't use pre/post increment as a value - just use by itself
    val yy = ++n // (yy = n + 1; n = n + 1)
    val zz = n++ // (yy = n; n = n + 1)

    println(n)

    val s1 = "Hello"
    val s2 = "Goodbye"
    var areSameString = (s1 === s2)
}