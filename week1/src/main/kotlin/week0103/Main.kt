package week0103

class Person(var name: String, var age: Int)
    // primary constructor

class Thing()
    // primary constructor

class Person2(var name: String, var age: Int) { // primary constructor
    constructor(age: Int): this("Nobody", age) // secondary constructor
}

class Person3(var name: String = "Nobody", var age: Int = 0) // primary constructor w def values

class Point(
    val x: Double,
    val y: Double,
    val z: Double,
)

fun main() {
    val person1 = Person("Scott", 56)
    val person2 = Person2(56)
    val person3a = Person3()
    val person3b = Person3("Scott", 56)
    val person3c = Person3("Scott")
    // val person3BAD = Person3(56) // no good!
    val person3d = Person3(age = 56)

    val point = Point(x = 10.0, y = 30.0, z = 30.0)
}