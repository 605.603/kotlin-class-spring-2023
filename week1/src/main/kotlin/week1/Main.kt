package week1

// property
var x: Int = 5  // READ/WRITE
    // gets a backing field
    // gets a getter
    //    get() { return field }
    // gets a setter
    //    set(value) { field = value }

val y: Int = 10 // READ ONLY
    // gets a backing field
    // gets a getter
    //    get() { return field }

var loggableX: Int = 10
//    // default impl of get()
//    get() {
//        return field // the backing field
//    }
    get() {
        println("reading loggableX")
        return field // the backing field
    }

val myName: String
    get() {
        // not referencing "field"
        //   NOT BACKING FIELD CREATED
        return "Scott"
    }

// NO backing field generated because "field" not mentioned in get/set
var nameInDatabase: String
    get() {
        // fetch value from database
        return "value from database"
    }
    set(value) {
        // store value from database
    }




fun main(args: Array<String>) {
    val field: Int = 42 // field isn't reserved - try not to do this though
    x = 10 // ok
    println(loggableX)
//    y = 20 // bad - read only


    val buttonName: String = "Foo"

    // BASIC STRING
    var name: String = "Scott"
    name = "Steve"

    // RAW STRING
    val copyright = """
        Copyright 2023 Scott Stanchfield
           All Rights Reserved
        Blah Blah Blah
    """.trimIndent()

    val copyright2 = """
        |Copyright 2023 Scott Stanchfield
        |   All Rights Reserved
        |Blah Blah Blah
    """.trimMargin()

    val copyright3 = """
        *Copyright 2023 Scott Stanchfield
        *   All Rights Reserved
        *   $buttonName
        *   Blah Blah Blah
    """.trimMargin("*")

    val buttonText1 = "Press the " + buttonName + " button to continue" // bad old JAVA like syntax
    val buttonText2 = "Press the $buttonName button to continue" // nicer kotlin idiom
    val buttonText3 = "Press the _${buttonName}_ button to continue" // nicer kotlin idiom
    val buttonText4 = "Press the ${buttonName.length} button to continue" // nicer kotlin idiom


    println(copyright)
    println(copyright2)
    println(copyright3)
}