package week0102

// ALL TYPES ARE PUBLIC BY DEFAULT IN KOTLIN
// ALL TYPES ARE FINAL BY DEFAULT IN KOTLIN
open class Person {
    var name: String = ""
    var age: Int = 0
}
class Person3 {
    var name: String? = null
    var age: Int = 0
}

class Driver: Person() {
    var preferredSpeed: Int = 75
    private var numberOfTickets: Int = 0
    fun gotTicket() {
        numberOfTickets++
    }
}

fun main() {
    var person1: Person = Person() // no "new" - NOT NULLABLE
    person1.name = "Scott"
    person1.age = 56

    person1 = Person()

    var person2: Person? = Person() // no "new" - NULLABLE
    person2!!.name = "Scott" // NON-NULL ASSERTION - TRY TO NOT USE MOST OF THE TIME
    // SMART-CAST
    person2.age = 56       // NON-NULL ASSERTION - TRY TO NOT USE MOST OF THE TIME
        // COMING BACK TO THE WARNING IN A BIT...

    person2 = Person()
    person2.name = "ABC"

    person2 = null
    person2?.name = "Scott"
    person2?.age = 56

    println(person2?.name)
    println(person2?.age)

    var person3: Person3? = Person3() // no "new" - NULLABLE
    person3!!.name = "Scott" // NON-NULL ASSERTION - TRY TO NOT USE MOST OF THE TIME
    person3!!.age = 56       // NON-NULL ASSERTION - TRY TO NOT USE MOST OF THE TIME
        // COMING BACK TO THE WARNING IN A BIT...

    person3 = Person3()
    person3?.name = null
    person3?.age = 56

    printStuff(person3)
}

fun printStuff(person: Person3?) {
    println(person?.name?.length)
    println(person?.name)
    println(person?.age)

    // smart cast to Person3 (non-null)
    //   because of "if"
    if (person != null) {
        println(person.name?.length)
        println(person.name)
        println(person.age)
    }

}


// IN JAVA
//   public
//   (package) - default
//   protected (me and subclasses and package)
//   private (me only)

// IN KOTLIN
//   public (default)
//   protected (me and subclasses)
//   internal (me and module that contains me)
//   private (me only)